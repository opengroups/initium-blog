using APIBlog.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();

// Add dependency injection for BlogPostRepository.
builder.Services.AddScoped<IBlogPostRepository, BlogPostRepository>();

// Add logging services.
builder.Logging.AddConsole();

// Configure Swagger/OpenAPI.
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();
app.MapControllers();

app.Run();
