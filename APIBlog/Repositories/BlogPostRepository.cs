﻿using APIBlog.Models;

namespace APIBlog.Repositories
{
    public class BlogPostRepository : IBlogPostRepository
    {
        // in-memory data var.
        private static List<BlogPost> _blogPosts = new List<BlogPost>();

        // return all records method
        public IEnumerable<BlogPost> GetAllBlogPosts()
        {
            return _blogPosts;
        }

        // add new record method
        public void AddBlogPost(BlogPost blogPost)
        {
            if (blogPost.Id == 0 )
            {
                // assign a new ID to the Id attribute
                blogPost.Id = _blogPosts.Count > 0 ? _blogPosts.Max(bp => bp.Id) + 1 : 1;
            }
            else
            {
                // validate the ID is already found
                bool isFound = _blogPosts.Any(bp => bp.Id == blogPost.Id);
                if (isFound)
                {
                    throw new InvalidOperationException($"The ID {blogPost.Id} is already used by another post.");
                }
            }

            _blogPosts.Add(blogPost);

        }
    }

}
