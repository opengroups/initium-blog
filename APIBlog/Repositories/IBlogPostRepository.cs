﻿using APIBlog.Models;

namespace APIBlog.Repositories
{
    public interface IBlogPostRepository
    {
        IEnumerable<BlogPost> GetAllBlogPosts();
        void AddBlogPost(BlogPost blogPost);
    }

}
