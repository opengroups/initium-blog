﻿using APIBlog.Models;
using APIBlog.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace APIBlog.Controllers
{
    [ApiController]
    [Route("api/blogpost")]
    public class BlogPostsController : ControllerBase
    {
        private readonly IBlogPostRepository _blogPostRepository;
        private readonly ILogger<BlogPostsController> _logger;

        // dependency injection via constructor
        public BlogPostsController(IBlogPostRepository blogPostRepository, ILogger<BlogPostsController> logger)
        {
            _blogPostRepository = blogPostRepository;
            _logger = logger;
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<BlogPost>> GetAllBlogPosts()
        {

            try
            {
                // return all records in memory store
                var blogPosts = _blogPostRepository.GetAllBlogPosts();

                return Ok(blogPosts);
            }
            catch (Exception ex)
            {
                // print the logger exception
                _logger.LogError(ex, "An error occurred: {ErrorMessage}", ex.Message);

                // return an error response
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred retrieving blog posts. Please check it and try again.");
            }

        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult AddBlogPost([FromBody] BlogPost blogPost)
        {
            try
            {
                // valid object blog post
                if (blogPost == null)
                {
                    return BadRequest("Invalid data. Please check it and try again.");
                }
                if (!ModelState.IsValid)
                {
                    // validation required fields 
                    return BadRequest(ModelState);
                }
                // add the object blog post to the repository
                _blogPostRepository.AddBlogPost(blogPost);

                // return success response
                return CreatedAtAction(nameof(GetAllBlogPosts), null);
            }
            catch (Exception ex)
            {
                // print the logger exception
                _logger.LogError(ex, "An error occurred: {ErrorMessage}", ex.Message);

                // return an error response
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred adding a blog post. Please check it and try again.");
            }
        }
    }
}
