
# API Documentation 
- This is the documentation for the Blog API. 
- It allows users to retrieve a list of blog posts and add new posts. 
- This project is a tecnical test for Initium Software. 

## Getting Started 
To get started with the API, follow these instructions: 
1. Clone the repository:
 ```bash 
   git clone https://github.com/your/repository.git
 ```
2. Install the dependencies:
```bash 
   cd project-directory
   dotnet restore
 ```
3. Run the API
```bash 
   dotnet run
```
4. The API will be accessible at `http://localhost:5000/api/blogpost` 
     or by Swagger UI `http://localhost:5000/swagger/index.html`.

## Endpoints

### Get All Blog Posts

-   **URL:** `/api/blogposts`
-   **Method:** GET
-   **Description:** Return all of blog posts.

### Add Blog Post

-   **URL:** `/api/blogposts`
-   **Method:** POST
-   **Description:** Add new blog post.

## Dependencies

-   .NET 6.0
-   Swashbuckle.AspNetCore (6.2.3)
-   Microsoft.Extensions.Logging (7.0.0)

## Author
- Daniel Depablos - danieldepablos@gmail.com
- May 2023
